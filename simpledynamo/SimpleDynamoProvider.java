package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.concurrent.ExecutionException;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;

import static android.content.ContentValues.TAG;

public class SimpleDynamoProvider extends ContentProvider {

	/*
		The port number of the android devices which will be acting as the servers for the simple dynamo
	 */
	static final String REMOTE_PORT0 = "11108";
	static final String REMOTE_PORT1 = "11112";
	static final String REMOTE_PORT2 = "11116";
	static final String REMOTE_PORT3 = "11120";
	static final String REMOTE_PORT4 = "11124";
	/* the port list is the information of all the node in the system. In dymano we try to follow the concept of zero hops
		which means every node contains the information of all the nodes in the systen such that a request can be directly
		forwarded to the correct node without additional hoping
	 */
	static final String[] port_list = {REMOTE_PORT0,REMOTE_PORT1, REMOTE_PORT2, REMOTE_PORT3, REMOTE_PORT4};
	static final int SERVER_PORT = 10000;
	static Node head = null;
	static Node tail = null;
	static int size=0;
	String myPort = "";



	public String getPosition(String file){

		try {
			String[] str = new String[2];
			str = file.split(":");
			String hash_key = genHash(str[0]);
			String msg = "";
			Node temp = head;
			do {
				if (temp.prev.data.compareTo(temp.data) > 0 && (hash_key.compareTo(temp.prev.data) > 0 || hash_key.compareTo(temp.data) <= 0)) {
					//Log.e(TAG, "Inside INSERT first if");
					msg = temp.id + ":" + temp.next.id + ":" + temp.next.next.id + ":" + str[0] + ":" + str[1];
					//Log.e(TAG, "Inside INSERT NODE LIST" + msg);
					break;
				} else if ((hash_key.compareTo(temp.data) <= 0) && (hash_key.compareTo(temp.prev.data) > 0)) {
					//Log.e(TAG, "Inside INSERT second if");
					msg = temp.id + ":" + temp.next.id + ":" + temp.next.next.id + ":" + str[0] + ":" + str[1];
					//Log.e(TAG, "Inside INSERT NODE LIST" + msg);
					break;
				}
				temp = temp.next;
			} while (temp != head);
			//Log.e(TAG,"MESSAGE SENT BY GETPOSITION METHOD"+msg);
			return msg;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		//Log.e(TAG,"INSIDE DELETE");
		//Log.e(TAG,"selection value for delete"+selection);
		if ((selection).compareTo("@") == 0) {
			File dir = Environment.getDataDirectory();
			String path = dir.getAbsolutePath();
			File myDir = getContext().getFilesDir();
			String[] filename = myDir.list();
			String value="";
			if(filename!=null) {
				for (int i = 0; i < filename.length; i++) {
					File file = new File(myDir, filename[i]);
					boolean deleted = file.delete();
					if(deleted){
						//Log.e(TAG,"File is deleted");
					}
					else{
						//Log.e(TAG,"File is not deleted");
					}

				}

			}
		}else {

			for (int i = 0; i < port_list.length; i++) {
				//Log.e(TAG,"Inside delete connecting to port"+port_list[i]);
				String remotePort = port_list[i];
				try {
					Socket socket_next = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
							(Integer.parseInt(remotePort)));
					PrintWriter out_next = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket_next.getOutputStream())), true);
					String msgToSend = "DELETE";
					if (out_next != null) {
						// Sending the message to the server
						//Log.e(TAG, "Sending to the server" + msgToSend);
						out_next.println(msgToSend);
						//Log.e(TAG, "Sent to the server" + msgToSend);
						out_next.flush();
					}


					socket_next.close();
				} catch (UnknownHostException e) {
					//Log.e(TAG, "ClientTask UnknownHostException");
				} catch (IOException e) {
					//Log.e(TAG, "ClientTask socket IOException" + e);
				}
			}


		}
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub


		//Log.e(TAG,"INSIDE INSERT");
		String filename = values.getAsString("key");
		String val = values.getAsString("value");
		String msg = getPosition(filename+":"+val);

		String[] msg_sent = msg.split(":");

		for(int i = 0 ; i < 3 ; i++){

			try{
				Socket socket_insert_new = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
						(Integer.parseInt(msg_sent[i])*2));
				//Log.e(TAG,"CONNECTING TO SERVER PORT FOR INSERT :"+msg_sent[i]);
				PrintWriter out_insert_new = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket_insert_new.getOutputStream())), true);
				String message = "INSERT_VALUE :"+msg_sent[3]+":"+msg_sent[4];

				if (out_insert_new != null) {
					//Log.e(TAG, "SENDING TO SERVER PORT" + msg_sent[i]);
					out_insert_new.println(message);
					//Log.e(TAG, "Sent to the server" + msg_sent[i]);
					//Log.e(TAG, "Sent to the server message " + message);
					out_insert_new.flush();
				}

				socket_insert_new.close();

			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}



		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub

		// This is the node initialisation . When an avd starts it has the list of the other avds in the system in order to reduce latency
		try {
			new NodeInsert().add_node("5554");
			new NodeInsert().add_node("5556");
			new NodeInsert().add_node("5558");
			new NodeInsert().add_node("5560");
			new NodeInsert().add_node("5562");
			new NodeInsert().print_chord();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		TelephonyManager tel = (TelephonyManager) this.getContext().getSystemService(Context.TELEPHONY_SERVICE);
		String portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);
		myPort = String.valueOf((Integer.parseInt(portStr) * 2));
		Integer node_id_int = Integer.parseInt(myPort) / 2;
		String node_id = Integer.toString(node_id_int);
		try {
			/*
			 * Create a server socket as well as a thread (AsyncTask) that listens on the server
			 * port.
			 *
			 * AsyncTask is a simplified thread construct that Android provides. Please make sure
			 * you know how it works by reading
			 * http://developer.android.com/reference/android/os/AsyncTask.html
			 */
			ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
			new ServerTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSocket);
		} catch (IOException e) {
			/*
			 * Log is a good way to debug your code. LogCat prints out all the messages that
			 * Log class writes.
			 *
			 * Please read http://developer.android.com/tools/debugging/debugging-projects.html
			 * and http://developer.android.com/tools/debugging/debugging-log.html
			 * for more information on debugging.
			 */

		}

		try {
			/* this handles whenever a node fails and wants to rejoin the system. The string rejoin separates the node intialisation
				process from a node failure and rejoin process
			 */
			new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "REJOIN:"+node_id , myPort).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}


		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
						String[] selectionArgs, String sortOrder) {
		MatrixCursor cursor = new MatrixCursor(new String[]{"key", "value"});
		FileInputStream in = null;
		if ((selection).compareTo("@") == 0) {
			try {
				File dir = Environment.getDataDirectory();
				String path = dir.getAbsolutePath();
				File myDir = new File("data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
				String[] filename = myDir.list();
				for (int i = 0; i < filename.length; i++) {
					in = getContext().openFileInput(filename[i]);
					InputStreamReader inputStreamReader = new InputStreamReader(in);
					BufferedReader br = new BufferedReader(inputStreamReader);
					String val = br.readLine();
					String[] val_split = val.split(":");
					Log.i("FILE VALUE", val_split[0]);
					cursor.addRow(new String[]{filename[i], val_split[0]});
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if((selection).compareTo("*") == 0){
			for (int i = 0; i < port_list.length; i++) {
				String remotePort = port_list[i];
				try {
					Socket socket_next = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
							(Integer.parseInt(remotePort)));
					PrintWriter out_next = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket_next.getOutputStream())), true);
					String msgToSend = "GLOBAL_DUMP";
					if (out_next != null) {
						out_next.println(msgToSend);
						out_next.flush();
					}

					BufferedReader in1 = new BufferedReader(new InputStreamReader(socket_next.getInputStream()));
					String message = in1.readLine();
					if(message != null) {
						String[] value = new String[2];
						String[] arrOfStr = message.split("/");
						if (arrOfStr[0].compareTo("*") != 0) {

							for (String a : arrOfStr) {
								value = a.split(":");
								cursor.addRow(new String[]{value[0], value[1]});
							}
						}
					}
					socket_next.close();

				} catch (UnknownHostException e) {
					Log.e(TAG, "ClientTask UnknownHostException");
				} catch (IOException e) {
					Log.e(TAG, "ClientTask socket IOException" + e);
				}
			}



		}else
		{
			String msg = getPosition(selection+":value");

			String[] msg_sent = msg.split(":");

			int max = 0;
			String val="";
			for(int i = 0 ; i < 3 ; i++){

				try{
					Socket socket_insert_new = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
							(Integer.parseInt(msg_sent[i])*2));
					PrintWriter out_insert_new = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket_insert_new.getOutputStream())), true);
					String message = "QUERY_VALUE :"+msg_sent[3];

					if (out_insert_new != null) {
						out_insert_new.println(message);
						out_insert_new.flush();
					}
					BufferedReader br = new BufferedReader(new InputStreamReader(socket_insert_new.getInputStream()));
					String message_rec = br.readLine();
					Log.e(TAG, "Received position content of file from server " + message_rec);
					if(message_rec!=null) {
						String[] str = new String[3];
						str = message_rec.split(":");
						if (Integer.parseInt(str[2]) > max) {
							max = Integer.parseInt(str[2]);
							val = str[1];
						}
					}

					socket_insert_new.close();

				} catch (UnknownHostException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			cursor.addRow(new String[]{selection, val});



		}
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
					  String[] selectionArgs) {
		return 0;
	}

	private String genHash(String input) throws NoSuchAlgorithmException {
		MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
		byte[] sha1Hash = sha1.digest(input.getBytes());
		Formatter formatter = new Formatter();
		for (byte b : sha1Hash) {
			formatter.format("%02x", b);
		}
		return formatter.toString();
	}

	private Uri buildUri(String scheme, String authority) {
		Uri.Builder uriBuilder = new Uri.Builder();
		uriBuilder.authority(authority);
		uriBuilder.scheme(scheme);
		return uriBuilder.build();
	}

	private class ServerTask extends AsyncTask<ServerSocket, String, Void> {

		@Override
		protected Void doInBackground(ServerSocket... sockets) {
			Context context = getContext();
			ServerSocket serverSocket = sockets[0];

			/*
			 * The methods below defines all the kind of server request the server obtains and processes it
			 * to onProgressUpdate().
			 *
			 */

			while (true) {
				try {

					Socket socket = serverSocket.accept();
					BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					String message = in.readLine();
					message = message.trim();

					/*
						The insert request write the data in the mapped hashed node along with context (version number) of the data
					*/
					if (message.contains("INSERT_VALUE")) {
						String[] str = message.split(":");
						int val_inc;

						try{
							FileInputStream fin = getContext().openFileInput(str[1]);
							InputStreamReader inputStreamReader = new InputStreamReader(fin);
							BufferedReader br = new BufferedReader(inputStreamReader);
							String val = br.readLine();
							String[] val_split = val.split(":");
							val_inc = Integer.parseInt(val_split[1]);
							val_inc++;
						}catch(FileNotFoundException e){
							val_inc = 1;
						}


						FileOutputStream outputStream;


						try {
							outputStream = getContext().openFileOutput(str[1], Context.MODE_PRIVATE);
							outputStream.write((str[2]+":"+val_inc).getBytes());
							outputStream.close();

						} catch (Exception e) {
						}

					}

					/*
						The query method retrives a data from a hashed node and sends it back to the client
						Since each node has the information of all the other nodes in the system it can be done using
						a single hop
					*/
					else if(message.contains("QUERY_VALUE")) {
						try {
							String val="";
							MatrixCursor cursor = new MatrixCursor(new String[]{"key", "value"});
							String[] msg = new String[2];
							msg = message.split(":");
							File dir = Environment.getDataDirectory();
							String path = dir.getAbsolutePath();
							File myDir = new File("data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
							FileInputStream in_1 = context.openFileInput(msg[1]);
							InputStreamReader inputStreamReader = new InputStreamReader(in_1);
							BufferedReader br = new BufferedReader(inputStreamReader);
							val = br.readLine();

							PrintWriter seq = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
							if (seq != null) {
								seq.write(msg[1]+":"+val);
								seq.flush();
							}

						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					/*
						A global dump deletes all the data from a particular node
					*/
					else if (message.contains("GLOBAL_DUMP")) {
						File dir = Environment.getDataDirectory();
						String path = dir.getAbsolutePath();
						File myDir = new File("data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
						String[] filename = myDir.list();
						String value="";
						if(filename!=null) {
							for (int i = 0; i < filename.length; i++) {
								FileInputStream name = context.openFileInput(filename[i]);
								InputStreamReader inputStreamReader = new InputStreamReader(name);
								BufferedReader br = new BufferedReader(inputStreamReader);
								String val = br.readLine();
								String[] val_split = val.split(":");
								val = filename[i] + ":" + val_split[0];
								value = val + "/" + value;
							}
						}
						else
							value = "*/";


						PrintWriter seq = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
						if (seq != null) {
							seq.write(value);
							seq.flush();
						}


					}

					/*
						The delete method only deletes a particular value from a node and all its replications
					 */
					else if(message.contains("DELETE")){
						Uri mUri = buildUri("content", "edu.buffalo.cse.cse486586.simpledynamo.provider");
						ContentResolver mContentResolver = getContext().getContentResolver();
						ContentValues mContentValues = new ContentValues();
						File dir = Environment.getDataDirectory();
						String path = dir.getAbsolutePath();
						File myDir = getContext().getFilesDir();
						String[] filename = myDir.list();
						String value="";
						if(filename!=null) {
							for (int i = 0; i < filename.length; i++) {
								File file = new File(myDir, filename[i]);
								boolean deleted = file.delete();
								if(deleted){
									Log.e(TAG,"File is deleted");
								}
								else{
									Log.e(TAG,"File is not deleted");
								}

							}
						}
					}
					/*
						When the server receives a request for failure rejoin it read all the content of the data from the
						latest context and sends it to the joined node
					*/
					else if(message.contains("FAILURE_REJOIN")){
						File dir = Environment.getDataDirectory();
						String path = dir.getAbsolutePath();
						File myDir = new File("data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
						String[] filename = myDir.list();
						String value_1="";
						if(filename!=null) {
							for (int i = 0; i < filename.length; i++) {
								FileInputStream name = context.openFileInput(filename[i]);
								InputStreamReader inputStreamReader = new InputStreamReader(name);
								BufferedReader br = new BufferedReader(inputStreamReader);
								String val = br.readLine();
								val = filename[i] + ":" + val;
								value_1 = val + "/" + value_1;
							}
						}
						else
							value_1 = "*/";

						PrintWriter seq = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
						if (seq != null) {
							seq.write(value_1);
							Log.e(TAG, "server sent file content to client" + value_1);
							seq.flush();
						}

					}


					socket.close();

				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}
	}

	public String checkRange(String key, String range){


		String msg="";
		//Log.e(TAG,"check range for :"+key);
		String[] msg_split = key.split(":");
		String[] port_range = range.split(":");

		for(int i = 0 ; i < 3 ; i++ ) {
			try {
				String hash_key = genHash(msg_split[0]);
				Node temp = head;
				do {

					if (temp.id.compareTo(port_range[i]) == 0) {
						if (temp.prev.data.compareTo(temp.data) > 0 && (hash_key.compareTo(temp.prev.data) > 0 || hash_key.compareTo(temp.data) <= 0)) {
							msg = msg_split[0] + ":" + msg_split[1] + ":" + msg_split[2];
							return msg;
						} else if ((hash_key.compareTo(temp.data) <= 0) && (hash_key.compareTo(temp.prev.data) > 0)) {
							msg = msg_split[0] + ":" + msg_split[1] + ":" + msg_split[2];
							return msg;
						}

					}

					temp = temp.next;

				} while (temp != head);

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}


		return null;
	}
	private class ClientTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... msgs) {

			String client_message;

			/*
				When the a rejoin request is sent it performs a major function of making sure it has all the object writes that it has
				missed during the failure. This is done by asking the right node to copy from them
			 */
			if(msgs[0].contains("REJOIN")) {
				String[] str = msgs[0].split(":");
				String msg = rejoin(str[1]);
				String[] port_no = msg.split(":");
				String port_range = str[1]+":"+port_no[2]+":"+port_no[3];

				/*
					The node after rejoining sends a request to all the nodes in the preferential list and updates the
					content of the failed node
				 */
				for(int i = 0 ; i < 4 ; i++) {
					try {

						Socket socket_next = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
								(Integer.parseInt(port_no[i])*2));

						PrintWriter out_next = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket_next.getOutputStream())), true);
						String msgToSend1 = "FAILURE_REJOIN";
						if (out_next != null) {
							out_next.println(msgToSend1);
							out_next.flush();
						}


						BufferedReader in1 = new BufferedReader(new InputStreamReader(socket_next.getInputStream()));
						String message = in1.readLine();
						Log.e(TAG, "Received content of file from failure server " + message);
						if(message!=null) {

								String[] value = new String[2];
								String[] arrOfStr = message.split("/");
								if (arrOfStr[0].compareTo("*") != 0) {

									for (String a : arrOfStr) {
										value = a.split(":");
										String final_val = value[1] + ":" + value[2];
										Log.e(TAG, "key :" + value[0] + "value :" + final_val);
										String s = value[0]+":"+final_val+":"+str[1];
										String result  = checkRange(s,port_range);
										Log.e(TAG,"Result of the check function :"+ result);

										if(result != null){

											String[] result_split = result.split(":");
											int val_inc;
											try{
												FileInputStream fin = getContext().openFileInput(result_split[0]);
												InputStreamReader inputStreamReader = new InputStreamReader(fin);
												BufferedReader br = new BufferedReader(inputStreamReader);
												String val = br.readLine();
												String[] val_split = val.split(":");
												val_inc = Integer.parseInt(val_split[1]);
												if(val_inc > Integer.parseInt(result_split[2])){
													result_split[1] = val_split[0];
													result_split[2] = val_split[1];
												}
											}catch(FileNotFoundException e){

											}


											FileOutputStream outputStream;


											try {
												outputStream = getContext().openFileOutput(result_split[0], Context.MODE_PRIVATE);
												outputStream.write((result_split[1]+":"+result_split[2]).getBytes());
												outputStream.close();

											} catch (Exception e) {
											}
										}

									}


								}

						}


							socket_next.close();


					} catch (UnknownHostException e) {
						//Log.e(TAG, "ClientTask UnknownHostException");
					} catch (IOException e) {
						//Log.e(TAG, "ClientTask socket IOException" + e);
					}

				}
			}

			return null;
		}

	}

	private String rejoin(String node){

		String msg="";
		Node temp = head;
		do{
			if(temp.id.compareTo(node)==0){
				msg=temp.next.id+":"+temp.next.next.id+":"+temp.prev.id+":"+temp.prev.prev.id;
			}
			temp=temp.next;
		}while(temp!=head);
		return msg;
	}

	/* creating a ring structure with the node using the hashed value of the node.
		The output range of the hash function is treated as a fixed circular ring or space .
		The largest hash value wraps around the smallest hash value. Consistent hashing will
		distribute the load across multiple storage host
	*/
	private class Node {
		String data;
		String id;
		Node prev;
		Node next;

		Node(String d) throws NoSuchAlgorithmException {
			data = genHash(d);
			id = d;
			prev = null;
			next = null;
		}

	}

	private class NodeInsert {

		public void add_node(String data) throws NoSuchAlgorithmException {
			Node new_node = new Node(data);

			if (head == null) {
				//Node new_node = new Node(data);
				System.out.println("Inserting the first node");
				new_node.next = new_node;
				new_node.prev = new_node;
				head = new_node;
				tail = head;
				size++;
				System.out.println("head value , pred, next " + head.data + head.next + head.prev);

			}

			else if (((head.data).compareTo(new_node.data)) > 0) {
				new_node.next = head;
				new_node.prev = tail;
				head.prev = new_node;
				tail.next = new_node;
				head = new_node;
				size++;

			} else if (((new_node.data).compareTo(tail.data)) > 0) {
				new_node.prev = tail;
				new_node.next = head;
				tail.next = new_node;
				tail = tail.next;
				head.prev=tail;
				size++;

			} else if (((new_node.data).compareTo(tail.data)) < 0 && ((new_node.data).compareTo(head.data)) > 0) {
				Node temp = head;
				System.out.println(new_node.data + temp.data + temp.next.data);
				do {
					System.out.println(new_node.data + temp.data + temp.next.data);
					if (((new_node.data).compareTo(temp.data) > 0 && ((new_node.data).compareTo(temp.next.data)) < 0)) {
						new_node.prev = temp;
						new_node.next = temp.next;
						temp.next.prev = new_node;
						temp.next = new_node;
						size++;
						System.out.println("head value , pred, next " + head.data + head.next + head.prev);
						System.out.println("Second node value , pred, next " + new_node.data + new_node.next + new_node.prev);
					}
					temp = temp.next;
					System.out.println("going for next loop" + temp.data + head.data);

				} while (temp.next != head);
			}


		}

		public void print_chord() {
			Node temp = head;
			do {
				temp = temp.next;
			} while (temp != head);
		}
	}
}

